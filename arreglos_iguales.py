# Determinar si dos arreglos son iguales porque tienen exactamente los mismos elementos 

A = [1]
B = [1, 1, 1]

noEncontro = False 
for valor in A:
    if valor not in B:
        noEncontro = True
        break 
        
if noEncontro == True:
    print ("Arreglos no tienen los mismos elementos y no son iguales")   
else :
    print ("Arreglos tienen los mismos elementos y no son iguales") 

# Probar tambien con este caso A=[1,2,2] y B=[1,1,2]
